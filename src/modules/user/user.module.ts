import { Module } from '@nestjs/common';
import { getPrototypes } from '@kerthin/utils';

const userCases = getPrototypes(`${__dirname}/use-cases/**/*.usecase{.ts,.js}`);
const controllers = getPrototypes(
  `${__dirname}/infra/http/*.controller{.ts,.js}`,
);
const repositories = getPrototypes(
  `${__dirname}/repositories/*.repository{.ts,.js}`,
);

@Module({
  controllers: [...controllers],
  providers: [...userCases, ...repositories],
})
export class UserModule {}
