import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { Controller, Post, Body } from '@nestjs/common';
import { UserDTO } from '../../dtos/user.dto';
import { CreateUserUseCase, CreateUserDTO } from '../../use-cases/create-user';

@ApiTags('User')
@Controller('users')
export default class UserController {
  constructor(private readonly createUserUseCase: CreateUserUseCase) {}

  @Post('/')
  @ApiResponse({ status: 201, type: UserDTO })
  create(@Body() data: CreateUserDTO) {
    return this.createUserUseCase.execute(data);
  }
}
