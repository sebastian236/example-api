import { Injectable } from '@nestjs/common';

import { UseCase } from '../../../../shared';
import { UserDTO } from '../../dtos/user.dto';
import { CreateUserDTO } from './create-user.dto';

type UseCaseResult = Promise<UserDTO>;

@Injectable()
export class CreateUserUseCase
  implements UseCase<CreateUserDTO, UseCaseResult>
{
  async execute(data: CreateUserDTO): UseCaseResult {
    return null;
  }
}
