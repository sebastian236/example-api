import { Module } from '@nestjs/common';
import { getPrototypes } from '@kerthin/utils';
import { DatabaseModule } from './shared';

const modules = getPrototypes(`${__dirname}/modules/**/*.module{.ts,.js}`);

@Module({
  imports: [DatabaseModule, ...modules],
})
export class AppModule {}
